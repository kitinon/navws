
package schemas.dynamics.microsoft.page.vendor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Vendor_List" type="{urn:microsoft-dynamics-schemas/page/vendor}Vendor_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorList"
})
@XmlRootElement(name = "UpdateMultiple_Result")
public class UpdateMultipleResult {

    @XmlElement(name = "Vendor_List", required = true)
    protected VendorList vendorList;

    /**
     * Gets the value of the vendorList property.
     * 
     * @return
     *     possible object is
     *     {@link VendorList }
     *     
     */
    public VendorList getVendorList() {
        return vendorList;
    }

    /**
     * Sets the value of the vendorList property.
     * 
     * @param value
     *     allowed object is
     *     {@link VendorList }
     *     
     */
    public void setVendorList(VendorList value) {
        this.vendorList = value;
    }

}
