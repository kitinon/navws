
package schemas.dynamics.microsoft.page.vendor;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Vendor_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Vendor_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Old_Code"/>
 *     &lt;enumeration value="Name"/>
 *     &lt;enumeration value="Address"/>
 *     &lt;enumeration value="Address_2"/>
 *     &lt;enumeration value="Post_Code"/>
 *     &lt;enumeration value="Country_Region_Code"/>
 *     &lt;enumeration value="Phone_No"/>
 *     &lt;enumeration value="Primary_Contact_No"/>
 *     &lt;enumeration value="Contact"/>
 *     &lt;enumeration value="City"/>
 *     &lt;enumeration value="Search_Name"/>
 *     &lt;enumeration value="Balance_LCY"/>
 *     &lt;enumeration value="Purchaser_Code"/>
 *     &lt;enumeration value="Responsibility_Center"/>
 *     &lt;enumeration value="Blocked"/>
 *     &lt;enumeration value="Last_Date_Modified"/>
 *     &lt;enumeration value="Fax_No"/>
 *     &lt;enumeration value="E_Mail"/>
 *     &lt;enumeration value="Home_Page"/>
 *     &lt;enumeration value="IC_Partner_Code"/>
 *     &lt;enumeration value="Pay_to_Vendor_No"/>
 *     &lt;enumeration value="Gen_Bus_Posting_Group"/>
 *     &lt;enumeration value="VAT_Bus_Posting_Group"/>
 *     &lt;enumeration value="Vendor_Posting_Group"/>
 *     &lt;enumeration value="Invoice_Disc_Code"/>
 *     &lt;enumeration value="Prices_Including_VAT"/>
 *     &lt;enumeration value="Prepayment_Percent"/>
 *     &lt;enumeration value="Cheque_Receive"/>
 *     &lt;enumeration value="Vendor_Bank_Account_Code"/>
 *     &lt;enumeration value="Application_Method"/>
 *     &lt;enumeration value="Payment_Terms_Code"/>
 *     &lt;enumeration value="Payment_Method_Code"/>
 *     &lt;enumeration value="Priority"/>
 *     &lt;enumeration value="Cash_Flow_Payment_Terms_Code"/>
 *     &lt;enumeration value="Our_Account_No"/>
 *     &lt;enumeration value="Block_Payment_Tolerance"/>
 *     &lt;enumeration value="Location_Code"/>
 *     &lt;enumeration value="Shipment_Method_Code"/>
 *     &lt;enumeration value="Lead_Time_Calculation"/>
 *     &lt;enumeration value="Base_Calendar_Code"/>
 *     &lt;enumeration value="Customized_Calendar"/>
 *     &lt;enumeration value="Currency_Code"/>
 *     &lt;enumeration value="Language_Code"/>
 *     &lt;enumeration value="VAT_Registration_No"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Vendor_Fields")
@XmlEnum
public enum VendorFields {

    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("Old_Code")
    OLD_CODE("Old_Code"),
    @XmlEnumValue("Name")
    NAME("Name"),
    @XmlEnumValue("Address")
    ADDRESS("Address"),
    @XmlEnumValue("Address_2")
    ADDRESS_2("Address_2"),
    @XmlEnumValue("Post_Code")
    POST_CODE("Post_Code"),
    @XmlEnumValue("Country_Region_Code")
    COUNTRY_REGION_CODE("Country_Region_Code"),
    @XmlEnumValue("Phone_No")
    PHONE_NO("Phone_No"),
    @XmlEnumValue("Primary_Contact_No")
    PRIMARY_CONTACT_NO("Primary_Contact_No"),
    @XmlEnumValue("Contact")
    CONTACT("Contact"),
    @XmlEnumValue("City")
    CITY("City"),
    @XmlEnumValue("Search_Name")
    SEARCH_NAME("Search_Name"),
    @XmlEnumValue("Balance_LCY")
    BALANCE_LCY("Balance_LCY"),
    @XmlEnumValue("Purchaser_Code")
    PURCHASER_CODE("Purchaser_Code"),
    @XmlEnumValue("Responsibility_Center")
    RESPONSIBILITY_CENTER("Responsibility_Center"),
    @XmlEnumValue("Blocked")
    BLOCKED("Blocked"),
    @XmlEnumValue("Last_Date_Modified")
    LAST_DATE_MODIFIED("Last_Date_Modified"),
    @XmlEnumValue("Fax_No")
    FAX_NO("Fax_No"),
    @XmlEnumValue("E_Mail")
    E_MAIL("E_Mail"),
    @XmlEnumValue("Home_Page")
    HOME_PAGE("Home_Page"),
    @XmlEnumValue("IC_Partner_Code")
    IC_PARTNER_CODE("IC_Partner_Code"),
    @XmlEnumValue("Pay_to_Vendor_No")
    PAY_TO_VENDOR_NO("Pay_to_Vendor_No"),
    @XmlEnumValue("Gen_Bus_Posting_Group")
    GEN_BUS_POSTING_GROUP("Gen_Bus_Posting_Group"),
    @XmlEnumValue("VAT_Bus_Posting_Group")
    VAT_BUS_POSTING_GROUP("VAT_Bus_Posting_Group"),
    @XmlEnumValue("Vendor_Posting_Group")
    VENDOR_POSTING_GROUP("Vendor_Posting_Group"),
    @XmlEnumValue("Invoice_Disc_Code")
    INVOICE_DISC_CODE("Invoice_Disc_Code"),
    @XmlEnumValue("Prices_Including_VAT")
    PRICES_INCLUDING_VAT("Prices_Including_VAT"),
    @XmlEnumValue("Prepayment_Percent")
    PREPAYMENT_PERCENT("Prepayment_Percent"),
    @XmlEnumValue("Cheque_Receive")
    CHEQUE_RECEIVE("Cheque_Receive"),
    @XmlEnumValue("Vendor_Bank_Account_Code")
    VENDOR_BANK_ACCOUNT_CODE("Vendor_Bank_Account_Code"),
    @XmlEnumValue("Application_Method")
    APPLICATION_METHOD("Application_Method"),
    @XmlEnumValue("Payment_Terms_Code")
    PAYMENT_TERMS_CODE("Payment_Terms_Code"),
    @XmlEnumValue("Payment_Method_Code")
    PAYMENT_METHOD_CODE("Payment_Method_Code"),
    @XmlEnumValue("Priority")
    PRIORITY("Priority"),
    @XmlEnumValue("Cash_Flow_Payment_Terms_Code")
    CASH_FLOW_PAYMENT_TERMS_CODE("Cash_Flow_Payment_Terms_Code"),
    @XmlEnumValue("Our_Account_No")
    OUR_ACCOUNT_NO("Our_Account_No"),
    @XmlEnumValue("Block_Payment_Tolerance")
    BLOCK_PAYMENT_TOLERANCE("Block_Payment_Tolerance"),
    @XmlEnumValue("Location_Code")
    LOCATION_CODE("Location_Code"),
    @XmlEnumValue("Shipment_Method_Code")
    SHIPMENT_METHOD_CODE("Shipment_Method_Code"),
    @XmlEnumValue("Lead_Time_Calculation")
    LEAD_TIME_CALCULATION("Lead_Time_Calculation"),
    @XmlEnumValue("Base_Calendar_Code")
    BASE_CALENDAR_CODE("Base_Calendar_Code"),
    @XmlEnumValue("Customized_Calendar")
    CUSTOMIZED_CALENDAR("Customized_Calendar"),
    @XmlEnumValue("Currency_Code")
    CURRENCY_CODE("Currency_Code"),
    @XmlEnumValue("Language_Code")
    LANGUAGE_CODE("Language_Code"),
    @XmlEnumValue("VAT_Registration_No")
    VAT_REGISTRATION_NO("VAT_Registration_No");
    private final String value;

    VendorFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VendorFields fromValue(String v) {
        for (VendorFields c: VendorFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
