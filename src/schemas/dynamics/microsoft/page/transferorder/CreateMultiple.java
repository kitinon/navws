
package schemas.dynamics.microsoft.page.transferorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferOrder_List" type="{urn:microsoft-dynamics-schemas/page/transferorder}TransferOrder_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferOrderList"
})
@XmlRootElement(name = "CreateMultiple")
public class CreateMultiple {

    @XmlElement(name = "TransferOrder_List", required = true)
    protected TransferOrderList transferOrderList;

    /**
     * Gets the value of the transferOrderList property.
     * 
     * @return
     *     possible object is
     *     {@link TransferOrderList }
     *     
     */
    public TransferOrderList getTransferOrderList() {
        return transferOrderList;
    }

    /**
     * Sets the value of the transferOrderList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferOrderList }
     *     
     */
    public void setTransferOrderList(TransferOrderList value) {
        this.transferOrderList = value;
    }

}
