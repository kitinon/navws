
package schemas.dynamics.microsoft.page.transferorder;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "TransferOrder_Service", targetNamespace = "urn:microsoft-dynamics-schemas/page/transferorder", wsdlLocation = "http://nav2013:7047/DynamicsNAV70/WS/HIQFOOD/Page/TransferOrder")
public class TransferOrderService
    extends Service
{

    private final static URL TRANSFERORDERSERVICE_WSDL_LOCATION;
    private final static WebServiceException TRANSFERORDERSERVICE_EXCEPTION;
    private final static QName TRANSFERORDERSERVICE_QNAME = new QName("urn:microsoft-dynamics-schemas/page/transferorder", "TransferOrder_Service");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://nav2013:7047/DynamicsNAV70/WS/HIQFOOD/Page/TransferOrder");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        TRANSFERORDERSERVICE_WSDL_LOCATION = url;
        TRANSFERORDERSERVICE_EXCEPTION = e;
    }

    public TransferOrderService() {
        super(__getWsdlLocation(), TRANSFERORDERSERVICE_QNAME);
    }

    public TransferOrderService(WebServiceFeature... features) {
        super(__getWsdlLocation(), TRANSFERORDERSERVICE_QNAME, features);
    }

    public TransferOrderService(URL wsdlLocation) {
        super(wsdlLocation, TRANSFERORDERSERVICE_QNAME);
    }

    public TransferOrderService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, TRANSFERORDERSERVICE_QNAME, features);
    }

    public TransferOrderService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TransferOrderService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns TransferOrderPort
     */
    @WebEndpoint(name = "TransferOrder_Port")
    public TransferOrderPort getTransferOrderPort() {
        return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/transferorder", "TransferOrder_Port"), TransferOrderPort.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TransferOrderPort
     */
    @WebEndpoint(name = "TransferOrder_Port")
    public TransferOrderPort getTransferOrderPort(WebServiceFeature... features) {
        return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/transferorder", "TransferOrder_Port"), TransferOrderPort.class, features);
    }

    private static URL __getWsdlLocation() {
        if (TRANSFERORDERSERVICE_EXCEPTION!= null) {
            throw TRANSFERORDERSERVICE_EXCEPTION;
        }
        return TRANSFERORDERSERVICE_WSDL_LOCATION;
    }

}
