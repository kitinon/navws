
package schemas.dynamics.microsoft.page.transferorder;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the schemas.dynamics.microsoft.page.transferorder package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: schemas.dynamics.microsoft.page.transferorder
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PostShipResult }
     * 
     */
    public PostShipResult createPostShipResult() {
        return new PostShipResult();
    }

    /**
     * Create an instance of {@link ReadByRecId }
     * 
     */
    public ReadByRecId createReadByRecId() {
        return new ReadByRecId();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link TransferOrder }
     * 
     */
    public TransferOrder createTransferOrder() {
        return new TransferOrder();
    }

    /**
     * Create an instance of {@link ReadByRecIdResult }
     * 
     */
    public ReadByRecIdResult createReadByRecIdResult() {
        return new ReadByRecIdResult();
    }

    /**
     * Create an instance of {@link PostShip }
     * 
     */
    public PostShip createPostShip() {
        return new PostShip();
    }

    /**
     * Create an instance of {@link CreateResult }
     * 
     */
    public CreateResult createCreateResult() {
        return new CreateResult();
    }

    /**
     * Create an instance of {@link GetRecIdFromKey }
     * 
     */
    public GetRecIdFromKey createGetRecIdFromKey() {
        return new GetRecIdFromKey();
    }

    /**
     * Create an instance of {@link GetRecIdFromKeyResult }
     * 
     */
    public GetRecIdFromKeyResult createGetRecIdFromKeyResult() {
        return new GetRecIdFromKeyResult();
    }

    /**
     * Create an instance of {@link UpdateMultiple }
     * 
     */
    public UpdateMultiple createUpdateMultiple() {
        return new UpdateMultiple();
    }

    /**
     * Create an instance of {@link TransferOrderList }
     * 
     */
    public TransferOrderList createTransferOrderList() {
        return new TransferOrderList();
    }

    /**
     * Create an instance of {@link ReadResult }
     * 
     */
    public ReadResult createReadResult() {
        return new ReadResult();
    }

    /**
     * Create an instance of {@link Create }
     * 
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link PostReceipt }
     * 
     */
    public PostReceipt createPostReceipt() {
        return new PostReceipt();
    }

    /**
     * Create an instance of {@link PostReceiptResult }
     * 
     */
    public PostReceiptResult createPostReceiptResult() {
        return new PostReceiptResult();
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link CreateMultipleResult }
     * 
     */
    public CreateMultipleResult createCreateMultipleResult() {
        return new CreateMultipleResult();
    }

    /**
     * Create an instance of {@link DeleteResult }
     * 
     */
    public DeleteResult createDeleteResult() {
        return new DeleteResult();
    }

    /**
     * Create an instance of {@link UpdateMultipleResult }
     * 
     */
    public UpdateMultipleResult createUpdateMultipleResult() {
        return new UpdateMultipleResult();
    }

    /**
     * Create an instance of {@link UpdateResult }
     * 
     */
    public UpdateResult createUpdateResult() {
        return new UpdateResult();
    }

    /**
     * Create an instance of {@link IsUpdatedResult }
     * 
     */
    public IsUpdatedResult createIsUpdatedResult() {
        return new IsUpdatedResult();
    }

    /**
     * Create an instance of {@link IsUpdated }
     * 
     */
    public IsUpdated createIsUpdated() {
        return new IsUpdated();
    }

    /**
     * Create an instance of {@link ReadMultipleResult }
     * 
     */
    public ReadMultipleResult createReadMultipleResult() {
        return new ReadMultipleResult();
    }

    /**
     * Create an instance of {@link DeleteTransferLines }
     * 
     */
    public DeleteTransferLines createDeleteTransferLines() {
        return new DeleteTransferLines();
    }

    /**
     * Create an instance of {@link Read }
     * 
     */
    public Read createRead() {
        return new Read();
    }

    /**
     * Create an instance of {@link CreateMultiple }
     * 
     */
    public CreateMultiple createCreateMultiple() {
        return new CreateMultiple();
    }

    /**
     * Create an instance of {@link ReadMultiple }
     * 
     */
    public ReadMultiple createReadMultiple() {
        return new ReadMultiple();
    }

    /**
     * Create an instance of {@link TransferOrderFilter }
     * 
     */
    public TransferOrderFilter createTransferOrderFilter() {
        return new TransferOrderFilter();
    }

    /**
     * Create an instance of {@link DeleteTransferLinesResult }
     * 
     */
    public DeleteTransferLinesResult createDeleteTransferLinesResult() {
        return new DeleteTransferLinesResult();
    }

    /**
     * Create an instance of {@link TransferOrderLine }
     * 
     */
    public TransferOrderLine createTransferOrderLine() {
        return new TransferOrderLine();
    }

    /**
     * Create an instance of {@link TransferOrderLineList }
     * 
     */
    public TransferOrderLineList createTransferOrderLineList() {
        return new TransferOrderLineList();
    }

}
