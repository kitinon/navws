
package schemas.dynamics.microsoft.page.transferorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferOrder" type="{urn:microsoft-dynamics-schemas/page/transferorder}TransferOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferOrder"
})
@XmlRootElement(name = "Create")
public class Create {

    @XmlElement(name = "TransferOrder", required = true)
    protected TransferOrder transferOrder;

    /**
     * Gets the value of the transferOrder property.
     * 
     * @return
     *     possible object is
     *     {@link TransferOrder }
     *     
     */
    public TransferOrder getTransferOrder() {
        return transferOrder;
    }

    /**
     * Sets the value of the transferOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferOrder }
     *     
     */
    public void setTransferOrder(TransferOrder value) {
        this.transferOrder = value;
    }

}
