
package schemas.dynamics.microsoft.page.productionordercomponent;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductionOrderComponent_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProductionOrderComponent_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Status"/>
 *     &lt;enumeration value="Prod_Order_No"/>
 *     &lt;enumeration value="Prod_Order_Line_No"/>
 *     &lt;enumeration value="Item_No"/>
 *     &lt;enumeration value="Variant_Code"/>
 *     &lt;enumeration value="Due_Date_Time"/>
 *     &lt;enumeration value="Due_Date"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Scrap_Percent"/>
 *     &lt;enumeration value="Calculation_Formula"/>
 *     &lt;enumeration value="Length"/>
 *     &lt;enumeration value="Width"/>
 *     &lt;enumeration value="Weight"/>
 *     &lt;enumeration value="Depth"/>
 *     &lt;enumeration value="Quantity_per"/>
 *     &lt;enumeration value="Reserved_Quantity"/>
 *     &lt;enumeration value="Unit_of_Measure_Code"/>
 *     &lt;enumeration value="Flushing_Method"/>
 *     &lt;enumeration value="Expected_Quantity"/>
 *     &lt;enumeration value="Remaining_Quantity"/>
 *     &lt;enumeration value="Shortcut_Dimension_1_Code"/>
 *     &lt;enumeration value="Shortcut_Dimension_2_Code"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_3_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_4_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_5_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_6_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_7_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_8_x005D_"/>
 *     &lt;enumeration value="Routing_Link_Code"/>
 *     &lt;enumeration value="Location_Code"/>
 *     &lt;enumeration value="Bin_Code"/>
 *     &lt;enumeration value="Unit_Cost"/>
 *     &lt;enumeration value="Cost_Amount"/>
 *     &lt;enumeration value="Position"/>
 *     &lt;enumeration value="Position_2"/>
 *     &lt;enumeration value="Position_3"/>
 *     &lt;enumeration value="Lead_Time_Offset"/>
 *     &lt;enumeration value="Qty_Picked"/>
 *     &lt;enumeration value="Qty_Picked_Base"/>
 *     &lt;enumeration value="Substitution_Available"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProductionOrderComponent_Fields")
@XmlEnum
public enum ProductionOrderComponentFields {

    @XmlEnumValue("Status")
    STATUS("Status"),
    @XmlEnumValue("Prod_Order_No")
    PROD_ORDER_NO("Prod_Order_No"),
    @XmlEnumValue("Prod_Order_Line_No")
    PROD_ORDER_LINE_NO("Prod_Order_Line_No"),
    @XmlEnumValue("Item_No")
    ITEM_NO("Item_No"),
    @XmlEnumValue("Variant_Code")
    VARIANT_CODE("Variant_Code"),
    @XmlEnumValue("Due_Date_Time")
    DUE_DATE_TIME("Due_Date_Time"),
    @XmlEnumValue("Due_Date")
    DUE_DATE("Due_Date"),
    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("Scrap_Percent")
    SCRAP_PERCENT("Scrap_Percent"),
    @XmlEnumValue("Calculation_Formula")
    CALCULATION_FORMULA("Calculation_Formula"),
    @XmlEnumValue("Length")
    LENGTH("Length"),
    @XmlEnumValue("Width")
    WIDTH("Width"),
    @XmlEnumValue("Weight")
    WEIGHT("Weight"),
    @XmlEnumValue("Depth")
    DEPTH("Depth"),
    @XmlEnumValue("Quantity_per")
    QUANTITY_PER("Quantity_per"),
    @XmlEnumValue("Reserved_Quantity")
    RESERVED_QUANTITY("Reserved_Quantity"),
    @XmlEnumValue("Unit_of_Measure_Code")
    UNIT_OF_MEASURE_CODE("Unit_of_Measure_Code"),
    @XmlEnumValue("Flushing_Method")
    FLUSHING_METHOD("Flushing_Method"),
    @XmlEnumValue("Expected_Quantity")
    EXPECTED_QUANTITY("Expected_Quantity"),
    @XmlEnumValue("Remaining_Quantity")
    REMAINING_QUANTITY("Remaining_Quantity"),
    @XmlEnumValue("Shortcut_Dimension_1_Code")
    SHORTCUT_DIMENSION_1_CODE("Shortcut_Dimension_1_Code"),
    @XmlEnumValue("Shortcut_Dimension_2_Code")
    SHORTCUT_DIMENSION_2_CODE("Shortcut_Dimension_2_Code"),
    @XmlEnumValue("ShortcutDimCode_x005B_3_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_3_X_005_D("ShortcutDimCode_x005B_3_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_4_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_4_X_005_D("ShortcutDimCode_x005B_4_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_5_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_5_X_005_D("ShortcutDimCode_x005B_5_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_6_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_6_X_005_D("ShortcutDimCode_x005B_6_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_7_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_7_X_005_D("ShortcutDimCode_x005B_7_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_8_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_8_X_005_D("ShortcutDimCode_x005B_8_x005D_"),
    @XmlEnumValue("Routing_Link_Code")
    ROUTING_LINK_CODE("Routing_Link_Code"),
    @XmlEnumValue("Location_Code")
    LOCATION_CODE("Location_Code"),
    @XmlEnumValue("Bin_Code")
    BIN_CODE("Bin_Code"),
    @XmlEnumValue("Unit_Cost")
    UNIT_COST("Unit_Cost"),
    @XmlEnumValue("Cost_Amount")
    COST_AMOUNT("Cost_Amount"),
    @XmlEnumValue("Position")
    POSITION("Position"),
    @XmlEnumValue("Position_2")
    POSITION_2("Position_2"),
    @XmlEnumValue("Position_3")
    POSITION_3("Position_3"),
    @XmlEnumValue("Lead_Time_Offset")
    LEAD_TIME_OFFSET("Lead_Time_Offset"),
    @XmlEnumValue("Qty_Picked")
    QTY_PICKED("Qty_Picked"),
    @XmlEnumValue("Qty_Picked_Base")
    QTY_PICKED_BASE("Qty_Picked_Base"),
    @XmlEnumValue("Substitution_Available")
    SUBSTITUTION_AVAILABLE("Substitution_Available");
    private final String value;

    ProductionOrderComponentFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProductionOrderComponentFields fromValue(String v) {
        for (ProductionOrderComponentFields c: ProductionOrderComponentFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
