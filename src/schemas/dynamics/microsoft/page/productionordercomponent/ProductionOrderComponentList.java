
package schemas.dynamics.microsoft.page.productionordercomponent;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductionOrderComponent_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductionOrderComponent_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductionOrderComponent" type="{urn:microsoft-dynamics-schemas/page/productionordercomponent}ProductionOrderComponent" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductionOrderComponent_List", propOrder = {
    "productionOrderComponent"
})
public class ProductionOrderComponentList {

    @XmlElement(name = "ProductionOrderComponent", required = true)
    protected List<ProductionOrderComponent> productionOrderComponent;

    /**
     * Gets the value of the productionOrderComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productionOrderComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductionOrderComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductionOrderComponent }
     * 
     * 
     */
    public List<ProductionOrderComponent> getProductionOrderComponent() {
        if (productionOrderComponent == null) {
            productionOrderComponent = new ArrayList<ProductionOrderComponent>();
        }
        return this.productionOrderComponent;
    }

}
