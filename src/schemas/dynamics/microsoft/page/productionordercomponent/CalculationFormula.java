
package schemas.dynamics.microsoft.page.productionordercomponent;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Calculation_Formula.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Calculation_Formula">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="_blank_"/>
 *     &lt;enumeration value="Length"/>
 *     &lt;enumeration value="Length__x002A__Width"/>
 *     &lt;enumeration value="Length__x002A__Width__x002A__Depth"/>
 *     &lt;enumeration value="Weight"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Calculation_Formula")
@XmlEnum
public enum CalculationFormula {

    @XmlEnumValue("_blank_")
    BLANK("_blank_"),
    @XmlEnumValue("Length")
    LENGTH("Length"),
    @XmlEnumValue("Length__x002A__Width")
    LENGTH_X_002_A_WIDTH("Length__x002A__Width"),
    @XmlEnumValue("Length__x002A__Width__x002A__Depth")
    LENGTH_X_002_A_WIDTH_X_002_A_DEPTH("Length__x002A__Width__x002A__Depth"),
    @XmlEnumValue("Weight")
    WEIGHT("Weight");
    private final String value;

    CalculationFormula(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CalculationFormula fromValue(String v) {
        for (CalculationFormula c: CalculationFormula.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
