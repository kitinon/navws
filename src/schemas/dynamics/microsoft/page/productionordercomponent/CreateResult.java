
package schemas.dynamics.microsoft.page.productionordercomponent;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductionOrderComponent" type="{urn:microsoft-dynamics-schemas/page/productionordercomponent}ProductionOrderComponent"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "productionOrderComponent"
})
@XmlRootElement(name = "Create_Result")
public class CreateResult {

    @XmlElement(name = "ProductionOrderComponent", required = true)
    protected ProductionOrderComponent productionOrderComponent;

    /**
     * Gets the value of the productionOrderComponent property.
     * 
     * @return
     *     possible object is
     *     {@link ProductionOrderComponent }
     *     
     */
    public ProductionOrderComponent getProductionOrderComponent() {
        return productionOrderComponent;
    }

    /**
     * Sets the value of the productionOrderComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionOrderComponent }
     *     
     */
    public void setProductionOrderComponent(ProductionOrderComponent value) {
        this.productionOrderComponent = value;
    }

}
