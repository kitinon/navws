
package schemas.dynamics.microsoft.page.productionordercomponent;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductionOrderComponent_List" type="{urn:microsoft-dynamics-schemas/page/productionordercomponent}ProductionOrderComponent_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "productionOrderComponentList"
})
@XmlRootElement(name = "UpdateMultiple")
public class UpdateMultiple {

    @XmlElement(name = "ProductionOrderComponent_List", required = true)
    protected ProductionOrderComponentList productionOrderComponentList;

    /**
     * Gets the value of the productionOrderComponentList property.
     * 
     * @return
     *     possible object is
     *     {@link ProductionOrderComponentList }
     *     
     */
    public ProductionOrderComponentList getProductionOrderComponentList() {
        return productionOrderComponentList;
    }

    /**
     * Sets the value of the productionOrderComponentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionOrderComponentList }
     *     
     */
    public void setProductionOrderComponentList(ProductionOrderComponentList value) {
        this.productionOrderComponentList = value;
    }

}
