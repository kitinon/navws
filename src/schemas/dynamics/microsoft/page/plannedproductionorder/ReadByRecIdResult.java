
package schemas.dynamics.microsoft.page.plannedproductionorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlannedProductionOrder" type="{urn:microsoft-dynamics-schemas/page/plannedproductionorder}PlannedProductionOrder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "plannedProductionOrder"
})
@XmlRootElement(name = "ReadByRecId_Result")
public class ReadByRecIdResult {

    @XmlElement(name = "PlannedProductionOrder")
    protected PlannedProductionOrder plannedProductionOrder;

    /**
     * Gets the value of the plannedProductionOrder property.
     * 
     * @return
     *     possible object is
     *     {@link PlannedProductionOrder }
     *     
     */
    public PlannedProductionOrder getPlannedProductionOrder() {
        return plannedProductionOrder;
    }

    /**
     * Sets the value of the plannedProductionOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlannedProductionOrder }
     *     
     */
    public void setPlannedProductionOrder(PlannedProductionOrder value) {
        this.plannedProductionOrder = value;
    }

}
