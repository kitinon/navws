
package schemas.dynamics.microsoft.page.plannedproductionorder;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlannedProductionOrder_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlannedProductionOrder_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Description_2"/>
 *     &lt;enumeration value="Source_Type"/>
 *     &lt;enumeration value="Source_No"/>
 *     &lt;enumeration value="Search_Description"/>
 *     &lt;enumeration value="Quantity"/>
 *     &lt;enumeration value="Due_Date"/>
 *     &lt;enumeration value="Assigned_User_ID"/>
 *     &lt;enumeration value="Last_Date_Modified"/>
 *     &lt;enumeration value="Starting_Time"/>
 *     &lt;enumeration value="Starting_Date"/>
 *     &lt;enumeration value="Ending_Time"/>
 *     &lt;enumeration value="Ending_Date"/>
 *     &lt;enumeration value="Inventory_Posting_Group"/>
 *     &lt;enumeration value="Gen_Prod_Posting_Group"/>
 *     &lt;enumeration value="Gen_Bus_Posting_Group"/>
 *     &lt;enumeration value="Shortcut_Dimension_1_Code"/>
 *     &lt;enumeration value="Shortcut_Dimension_2_Code"/>
 *     &lt;enumeration value="Location_Code"/>
 *     &lt;enumeration value="Bin_Code"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PlannedProductionOrder_Fields")
@XmlEnum
public enum PlannedProductionOrderFields {

    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("Description_2")
    DESCRIPTION_2("Description_2"),
    @XmlEnumValue("Source_Type")
    SOURCE_TYPE("Source_Type"),
    @XmlEnumValue("Source_No")
    SOURCE_NO("Source_No"),
    @XmlEnumValue("Search_Description")
    SEARCH_DESCRIPTION("Search_Description"),
    @XmlEnumValue("Quantity")
    QUANTITY("Quantity"),
    @XmlEnumValue("Due_Date")
    DUE_DATE("Due_Date"),
    @XmlEnumValue("Assigned_User_ID")
    ASSIGNED_USER_ID("Assigned_User_ID"),
    @XmlEnumValue("Last_Date_Modified")
    LAST_DATE_MODIFIED("Last_Date_Modified"),
    @XmlEnumValue("Starting_Time")
    STARTING_TIME("Starting_Time"),
    @XmlEnumValue("Starting_Date")
    STARTING_DATE("Starting_Date"),
    @XmlEnumValue("Ending_Time")
    ENDING_TIME("Ending_Time"),
    @XmlEnumValue("Ending_Date")
    ENDING_DATE("Ending_Date"),
    @XmlEnumValue("Inventory_Posting_Group")
    INVENTORY_POSTING_GROUP("Inventory_Posting_Group"),
    @XmlEnumValue("Gen_Prod_Posting_Group")
    GEN_PROD_POSTING_GROUP("Gen_Prod_Posting_Group"),
    @XmlEnumValue("Gen_Bus_Posting_Group")
    GEN_BUS_POSTING_GROUP("Gen_Bus_Posting_Group"),
    @XmlEnumValue("Shortcut_Dimension_1_Code")
    SHORTCUT_DIMENSION_1_CODE("Shortcut_Dimension_1_Code"),
    @XmlEnumValue("Shortcut_Dimension_2_Code")
    SHORTCUT_DIMENSION_2_CODE("Shortcut_Dimension_2_Code"),
    @XmlEnumValue("Location_Code")
    LOCATION_CODE("Location_Code"),
    @XmlEnumValue("Bin_Code")
    BIN_CODE("Bin_Code");
    private final String value;

    PlannedProductionOrderFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlannedProductionOrderFields fromValue(String v) {
        for (PlannedProductionOrderFields c: PlannedProductionOrderFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
