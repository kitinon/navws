
package schemas.dynamics.microsoft.page.plannedproductionorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Delete_ProdOrderLines_Result" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteProdOrderLinesResult"
})
@XmlRootElement(name = "Delete_ProdOrderLines_Result")
public class DeleteProdOrderLinesResult {

    @XmlElement(name = "Delete_ProdOrderLines_Result")
    protected boolean deleteProdOrderLinesResult;

    /**
     * Gets the value of the deleteProdOrderLinesResult property.
     * 
     */
    public boolean isDeleteProdOrderLinesResult() {
        return deleteProdOrderLinesResult;
    }

    /**
     * Sets the value of the deleteProdOrderLinesResult property.
     * 
     */
    public void setDeleteProdOrderLinesResult(boolean value) {
        this.deleteProdOrderLinesResult = value;
    }

}
