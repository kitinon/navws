
package schemas.dynamics.microsoft.page.plannedproductionorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlannedProductionOrder_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlannedProductionOrder_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlannedProductionOrder" type="{urn:microsoft-dynamics-schemas/page/plannedproductionorder}PlannedProductionOrder" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlannedProductionOrder_List", propOrder = {
    "plannedProductionOrder"
})
public class PlannedProductionOrderList {

    @XmlElement(name = "PlannedProductionOrder", required = true)
    protected List<PlannedProductionOrder> plannedProductionOrder;

    /**
     * Gets the value of the plannedProductionOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the plannedProductionOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlannedProductionOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlannedProductionOrder }
     * 
     * 
     */
    public List<PlannedProductionOrder> getPlannedProductionOrder() {
        if (plannedProductionOrder == null) {
            plannedProductionOrder = new ArrayList<PlannedProductionOrder>();
        }
        return this.plannedProductionOrder;
    }

}
