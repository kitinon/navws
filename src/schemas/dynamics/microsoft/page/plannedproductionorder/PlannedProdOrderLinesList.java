
package schemas.dynamics.microsoft.page.plannedproductionorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Planned_Prod_Order_Lines_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Planned_Prod_Order_Lines_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Planned_Prod_Order_Lines" type="{urn:microsoft-dynamics-schemas/page/plannedproductionorder}Planned_Prod_Order_Lines" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Planned_Prod_Order_Lines_List", propOrder = {
    "plannedProdOrderLines"
})
public class PlannedProdOrderLinesList {

    @XmlElement(name = "Planned_Prod_Order_Lines", required = true)
    protected List<PlannedProdOrderLines> plannedProdOrderLines;

    /**
     * Gets the value of the plannedProdOrderLines property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the plannedProdOrderLines property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlannedProdOrderLines().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlannedProdOrderLines }
     * 
     * 
     */
    public List<PlannedProdOrderLines> getPlannedProdOrderLines() {
        if (plannedProdOrderLines == null) {
            plannedProdOrderLines = new ArrayList<PlannedProdOrderLines>();
        }
        return this.plannedProdOrderLines;
    }

}
