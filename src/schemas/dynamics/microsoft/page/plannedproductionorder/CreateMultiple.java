
package schemas.dynamics.microsoft.page.plannedproductionorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlannedProductionOrder_List" type="{urn:microsoft-dynamics-schemas/page/plannedproductionorder}PlannedProductionOrder_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "plannedProductionOrderList"
})
@XmlRootElement(name = "CreateMultiple")
public class CreateMultiple {

    @XmlElement(name = "PlannedProductionOrder_List", required = true)
    protected PlannedProductionOrderList plannedProductionOrderList;

    /**
     * Gets the value of the plannedProductionOrderList property.
     * 
     * @return
     *     possible object is
     *     {@link PlannedProductionOrderList }
     *     
     */
    public PlannedProductionOrderList getPlannedProductionOrderList() {
        return plannedProductionOrderList;
    }

    /**
     * Sets the value of the plannedProductionOrderList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlannedProductionOrderList }
     *     
     */
    public void setPlannedProductionOrderList(PlannedProductionOrderList value) {
        this.plannedProductionOrderList = value;
    }

}
