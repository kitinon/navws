
package schemas.dynamics.microsoft.page.reqworksheet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentJnlBatchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentJnlBatchName",
    "recId"
})
@XmlRootElement(name = "ReadByRecId")
public class ReadByRecId {

    @XmlElement(name = "CurrentJnlBatchName", required = true)
    protected String currentJnlBatchName;
    @XmlElement(required = true)
    protected String recId;

    /**
     * Gets the value of the currentJnlBatchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentJnlBatchName() {
        return currentJnlBatchName;
    }

    /**
     * Sets the value of the currentJnlBatchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentJnlBatchName(String value) {
        this.currentJnlBatchName = value;
    }

    /**
     * Gets the value of the recId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecId() {
        return recId;
    }

    /**
     * Sets the value of the recId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecId(String value) {
        this.recId = value;
    }

}
