
package schemas.dynamics.microsoft.page.reqworksheet;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReqWorksheet_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReqWorksheet_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReqWorksheet" type="{urn:microsoft-dynamics-schemas/page/reqworksheet}ReqWorksheet" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReqWorksheet_List", propOrder = {
    "reqWorksheet"
})
public class ReqWorksheetList {

    @XmlElement(name = "ReqWorksheet", required = true)
    protected List<ReqWorksheet> reqWorksheet;

    /**
     * Gets the value of the reqWorksheet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reqWorksheet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReqWorksheet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReqWorksheet }
     * 
     * 
     */
    public List<ReqWorksheet> getReqWorksheet() {
        if (reqWorksheet == null) {
            reqWorksheet = new ArrayList<ReqWorksheet>();
        }
        return this.reqWorksheet;
    }

}
