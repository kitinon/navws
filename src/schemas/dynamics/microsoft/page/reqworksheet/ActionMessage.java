
package schemas.dynamics.microsoft.page.reqworksheet;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Action_Message.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Action_Message">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="_blank_"/>
 *     &lt;enumeration value="New"/>
 *     &lt;enumeration value="Change_Qty"/>
 *     &lt;enumeration value="Reschedule"/>
 *     &lt;enumeration value="Resched__x0026__Chg_Qty"/>
 *     &lt;enumeration value="Cancel"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Action_Message")
@XmlEnum
public enum ActionMessage {

    @XmlEnumValue("_blank_")
    BLANK("_blank_"),
    @XmlEnumValue("New")
    NEW("New"),
    @XmlEnumValue("Change_Qty")
    CHANGE_QTY("Change_Qty"),
    @XmlEnumValue("Reschedule")
    RESCHEDULE("Reschedule"),
    @XmlEnumValue("Resched__x0026__Chg_Qty")
    RESCHED_X_0026_CHG_QTY("Resched__x0026__Chg_Qty"),
    @XmlEnumValue("Cancel")
    CANCEL("Cancel");
    private final String value;

    ActionMessage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionMessage fromValue(String v) {
        for (ActionMessage c: ActionMessage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
