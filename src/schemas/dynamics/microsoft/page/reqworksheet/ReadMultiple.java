
package schemas.dynamics.microsoft.page.reqworksheet;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentJnlBatchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filter" type="{urn:microsoft-dynamics-schemas/page/reqworksheet}ReqWorksheet_Filter" maxOccurs="unbounded"/>
 *         &lt;element name="bookmarkKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="setSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentJnlBatchName",
    "filter",
    "bookmarkKey",
    "setSize"
})
@XmlRootElement(name = "ReadMultiple")
public class ReadMultiple {

    @XmlElement(name = "CurrentJnlBatchName", required = true)
    protected String currentJnlBatchName;
    @XmlElement(required = true)
    protected List<ReqWorksheetFilter> filter;
    protected String bookmarkKey;
    protected int setSize;

    /**
     * Gets the value of the currentJnlBatchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentJnlBatchName() {
        return currentJnlBatchName;
    }

    /**
     * Sets the value of the currentJnlBatchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentJnlBatchName(String value) {
        this.currentJnlBatchName = value;
    }

    /**
     * Gets the value of the filter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReqWorksheetFilter }
     * 
     * 
     */
    public List<ReqWorksheetFilter> getFilter() {
        if (filter == null) {
            filter = new ArrayList<ReqWorksheetFilter>();
        }
        return this.filter;
    }

    /**
     * Gets the value of the bookmarkKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookmarkKey() {
        return bookmarkKey;
    }

    /**
     * Sets the value of the bookmarkKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookmarkKey(String value) {
        this.bookmarkKey = value;
    }

    /**
     * Gets the value of the setSize property.
     * 
     */
    public int getSetSize() {
        return setSize;
    }

    /**
     * Sets the value of the setSize property.
     * 
     */
    public void setSetSize(int value) {
        this.setSize = value;
    }

}
