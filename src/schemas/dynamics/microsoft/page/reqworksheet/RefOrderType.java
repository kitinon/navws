
package schemas.dynamics.microsoft.page.reqworksheet;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ref_Order_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Ref_Order_Type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="_blank_"/>
 *     &lt;enumeration value="Purchase"/>
 *     &lt;enumeration value="Prod_Order"/>
 *     &lt;enumeration value="Transfer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Ref_Order_Type")
@XmlEnum
public enum RefOrderType {

    @XmlEnumValue("_blank_")
    BLANK("_blank_"),
    @XmlEnumValue("Purchase")
    PURCHASE("Purchase"),
    @XmlEnumValue("Prod_Order")
    PROD_ORDER("Prod_Order"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer");
    private final String value;

    RefOrderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RefOrderType fromValue(String v) {
        for (RefOrderType c: RefOrderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
