
package schemas.dynamics.microsoft.page.reqworksheet;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReqWorksheet_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReqWorksheet_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Type"/>
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Action_Message"/>
 *     &lt;enumeration value="Accept_Action_Message"/>
 *     &lt;enumeration value="Variant_Code"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Description_2"/>
 *     &lt;enumeration value="Transfer_from_Code"/>
 *     &lt;enumeration value="Location_Code"/>
 *     &lt;enumeration value="Original_Quantity"/>
 *     &lt;enumeration value="Quantity"/>
 *     &lt;enumeration value="Unit_of_Measure_Code"/>
 *     &lt;enumeration value="Direct_Unit_Cost"/>
 *     &lt;enumeration value="Currency_Code"/>
 *     &lt;enumeration value="Line_Discount_Percent"/>
 *     &lt;enumeration value="Original_Due_Date"/>
 *     &lt;enumeration value="Due_Date"/>
 *     &lt;enumeration value="Order_Date"/>
 *     &lt;enumeration value="Vendor_No"/>
 *     &lt;enumeration value="Vendor_Item_No"/>
 *     &lt;enumeration value="Order_Address_Code"/>
 *     &lt;enumeration value="Sell_to_Customer_No"/>
 *     &lt;enumeration value="Ship_to_Code"/>
 *     &lt;enumeration value="Prod_Order_No"/>
 *     &lt;enumeration value="Requester_ID"/>
 *     &lt;enumeration value="Confirmed"/>
 *     &lt;enumeration value="Shortcut_Dimension_1_Code"/>
 *     &lt;enumeration value="Shortcut_Dimension_2_Code"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_3_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_4_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_5_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_6_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_7_x005D_"/>
 *     &lt;enumeration value="ShortcutDimCode_x005B_8_x005D_"/>
 *     &lt;enumeration value="Ref_Order_No"/>
 *     &lt;enumeration value="Ref_Order_Type"/>
 *     &lt;enumeration value="Replenishment_System"/>
 *     &lt;enumeration value="Ref_Line_No"/>
 *     &lt;enumeration value="Planning_Flexibility"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReqWorksheet_Fields")
@XmlEnum
public enum ReqWorksheetFields {

    @XmlEnumValue("Type")
    TYPE("Type"),
    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("Action_Message")
    ACTION_MESSAGE("Action_Message"),
    @XmlEnumValue("Accept_Action_Message")
    ACCEPT_ACTION_MESSAGE("Accept_Action_Message"),
    @XmlEnumValue("Variant_Code")
    VARIANT_CODE("Variant_Code"),
    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("Description_2")
    DESCRIPTION_2("Description_2"),
    @XmlEnumValue("Transfer_from_Code")
    TRANSFER_FROM_CODE("Transfer_from_Code"),
    @XmlEnumValue("Location_Code")
    LOCATION_CODE("Location_Code"),
    @XmlEnumValue("Original_Quantity")
    ORIGINAL_QUANTITY("Original_Quantity"),
    @XmlEnumValue("Quantity")
    QUANTITY("Quantity"),
    @XmlEnumValue("Unit_of_Measure_Code")
    UNIT_OF_MEASURE_CODE("Unit_of_Measure_Code"),
    @XmlEnumValue("Direct_Unit_Cost")
    DIRECT_UNIT_COST("Direct_Unit_Cost"),
    @XmlEnumValue("Currency_Code")
    CURRENCY_CODE("Currency_Code"),
    @XmlEnumValue("Line_Discount_Percent")
    LINE_DISCOUNT_PERCENT("Line_Discount_Percent"),
    @XmlEnumValue("Original_Due_Date")
    ORIGINAL_DUE_DATE("Original_Due_Date"),
    @XmlEnumValue("Due_Date")
    DUE_DATE("Due_Date"),
    @XmlEnumValue("Order_Date")
    ORDER_DATE("Order_Date"),
    @XmlEnumValue("Vendor_No")
    VENDOR_NO("Vendor_No"),
    @XmlEnumValue("Vendor_Item_No")
    VENDOR_ITEM_NO("Vendor_Item_No"),
    @XmlEnumValue("Order_Address_Code")
    ORDER_ADDRESS_CODE("Order_Address_Code"),
    @XmlEnumValue("Sell_to_Customer_No")
    SELL_TO_CUSTOMER_NO("Sell_to_Customer_No"),
    @XmlEnumValue("Ship_to_Code")
    SHIP_TO_CODE("Ship_to_Code"),
    @XmlEnumValue("Prod_Order_No")
    PROD_ORDER_NO("Prod_Order_No"),
    @XmlEnumValue("Requester_ID")
    REQUESTER_ID("Requester_ID"),
    @XmlEnumValue("Confirmed")
    CONFIRMED("Confirmed"),
    @XmlEnumValue("Shortcut_Dimension_1_Code")
    SHORTCUT_DIMENSION_1_CODE("Shortcut_Dimension_1_Code"),
    @XmlEnumValue("Shortcut_Dimension_2_Code")
    SHORTCUT_DIMENSION_2_CODE("Shortcut_Dimension_2_Code"),
    @XmlEnumValue("ShortcutDimCode_x005B_3_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_3_X_005_D("ShortcutDimCode_x005B_3_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_4_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_4_X_005_D("ShortcutDimCode_x005B_4_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_5_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_5_X_005_D("ShortcutDimCode_x005B_5_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_6_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_6_X_005_D("ShortcutDimCode_x005B_6_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_7_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_7_X_005_D("ShortcutDimCode_x005B_7_x005D_"),
    @XmlEnumValue("ShortcutDimCode_x005B_8_x005D_")
    SHORTCUT_DIM_CODE_X_005_B_8_X_005_D("ShortcutDimCode_x005B_8_x005D_"),
    @XmlEnumValue("Ref_Order_No")
    REF_ORDER_NO("Ref_Order_No"),
    @XmlEnumValue("Ref_Order_Type")
    REF_ORDER_TYPE("Ref_Order_Type"),
    @XmlEnumValue("Replenishment_System")
    REPLENISHMENT_SYSTEM("Replenishment_System"),
    @XmlEnumValue("Ref_Line_No")
    REF_LINE_NO("Ref_Line_No"),
    @XmlEnumValue("Planning_Flexibility")
    PLANNING_FLEXIBILITY("Planning_Flexibility");
    private final String value;

    ReqWorksheetFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReqWorksheetFields fromValue(String v) {
        for (ReqWorksheetFields c: ReqWorksheetFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
