
package schemas.dynamics.microsoft.page.reqworksheet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReqWorksheet" type="{urn:microsoft-dynamics-schemas/page/reqworksheet}ReqWorksheet"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqWorksheet"
})
@XmlRootElement(name = "Create_Result")
public class CreateResult {

    @XmlElement(name = "ReqWorksheet", required = true)
    protected ReqWorksheet reqWorksheet;

    /**
     * Gets the value of the reqWorksheet property.
     * 
     * @return
     *     possible object is
     *     {@link ReqWorksheet }
     *     
     */
    public ReqWorksheet getReqWorksheet() {
        return reqWorksheet;
    }

    /**
     * Sets the value of the reqWorksheet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqWorksheet }
     *     
     */
    public void setReqWorksheet(ReqWorksheet value) {
        this.reqWorksheet = value;
    }

}
