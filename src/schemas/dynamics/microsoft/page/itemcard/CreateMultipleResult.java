
package schemas.dynamics.microsoft.page.itemcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemCard_List" type="{urn:microsoft-dynamics-schemas/page/itemcard}ItemCard_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemCardList"
})
@XmlRootElement(name = "CreateMultiple_Result")
public class CreateMultipleResult {

    @XmlElement(name = "ItemCard_List", required = true)
    protected ItemCardList itemCardList;

    /**
     * Gets the value of the itemCardList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCardList }
     *     
     */
    public ItemCardList getItemCardList() {
        return itemCardList;
    }

    /**
     * Sets the value of the itemCardList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCardList }
     *     
     */
    public void setItemCardList(ItemCardList value) {
        this.itemCardList = value;
    }

}
