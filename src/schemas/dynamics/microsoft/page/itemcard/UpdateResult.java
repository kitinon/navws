
package schemas.dynamics.microsoft.page.itemcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemCard" type="{urn:microsoft-dynamics-schemas/page/itemcard}ItemCard"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemCard"
})
@XmlRootElement(name = "Update_Result")
public class UpdateResult {

    @XmlElement(name = "ItemCard", required = true)
    protected ItemCard itemCard;

    /**
     * Gets the value of the itemCard property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCard }
     *     
     */
    public ItemCard getItemCard() {
        return itemCard;
    }

    /**
     * Sets the value of the itemCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCard }
     *     
     */
    public void setItemCard(ItemCard value) {
        this.itemCard = value;
    }

}
