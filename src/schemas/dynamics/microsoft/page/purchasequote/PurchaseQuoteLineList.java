
package schemas.dynamics.microsoft.page.purchasequote;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Purchase_Quote_Line_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Purchase_Quote_Line_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Purchase_Quote_Line" type="{urn:microsoft-dynamics-schemas/page/purchasequote}Purchase_Quote_Line" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Purchase_Quote_Line_List", propOrder = {
    "purchaseQuoteLine"
})
public class PurchaseQuoteLineList {

    @XmlElement(name = "Purchase_Quote_Line", required = true)
    protected List<PurchaseQuoteLine> purchaseQuoteLine;

    /**
     * Gets the value of the purchaseQuoteLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseQuoteLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseQuoteLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseQuoteLine }
     * 
     * 
     */
    public List<PurchaseQuoteLine> getPurchaseQuoteLine() {
        if (purchaseQuoteLine == null) {
            purchaseQuoteLine = new ArrayList<PurchaseQuoteLine>();
        }
        return this.purchaseQuoteLine;
    }

}
