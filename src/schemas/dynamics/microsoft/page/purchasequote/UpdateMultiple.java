
package schemas.dynamics.microsoft.page.purchasequote;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseQuote_List" type="{urn:microsoft-dynamics-schemas/page/purchasequote}PurchaseQuote_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "purchaseQuoteList"
})
@XmlRootElement(name = "UpdateMultiple")
public class UpdateMultiple {

    @XmlElement(name = "PurchaseQuote_List", required = true)
    protected PurchaseQuoteList purchaseQuoteList;

    /**
     * Gets the value of the purchaseQuoteList property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseQuoteList }
     *     
     */
    public PurchaseQuoteList getPurchaseQuoteList() {
        return purchaseQuoteList;
    }

    /**
     * Sets the value of the purchaseQuoteList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseQuoteList }
     *     
     */
    public void setPurchaseQuoteList(PurchaseQuoteList value) {
        this.purchaseQuoteList = value;
    }

}
