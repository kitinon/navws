
package schemas.dynamics.microsoft.page.purchasequote;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseQuote" type="{urn:microsoft-dynamics-schemas/page/purchasequote}PurchaseQuote"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "purchaseQuote"
})
@XmlRootElement(name = "Update")
public class Update {

    @XmlElement(name = "PurchaseQuote", required = true)
    protected PurchaseQuote purchaseQuote;

    /**
     * Gets the value of the purchaseQuote property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseQuote }
     *     
     */
    public PurchaseQuote getPurchaseQuote() {
        return purchaseQuote;
    }

    /**
     * Sets the value of the purchaseQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseQuote }
     *     
     */
    public void setPurchaseQuote(PurchaseQuote value) {
        this.purchaseQuote = value;
    }

}
