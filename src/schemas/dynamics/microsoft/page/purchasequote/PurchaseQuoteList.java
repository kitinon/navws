
package schemas.dynamics.microsoft.page.purchasequote;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaseQuote_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseQuote_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseQuote" type="{urn:microsoft-dynamics-schemas/page/purchasequote}PurchaseQuote" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseQuote_List", propOrder = {
    "purchaseQuote"
})
public class PurchaseQuoteList {

    @XmlElement(name = "PurchaseQuote", required = true)
    protected List<PurchaseQuote> purchaseQuote;

    /**
     * Gets the value of the purchaseQuote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseQuote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseQuote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseQuote }
     * 
     * 
     */
    public List<PurchaseQuote> getPurchaseQuote() {
        if (purchaseQuote == null) {
            purchaseQuote = new ArrayList<PurchaseQuote>();
        }
        return this.purchaseQuote;
    }

}
