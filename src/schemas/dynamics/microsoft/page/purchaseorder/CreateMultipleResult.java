
package schemas.dynamics.microsoft.page.purchaseorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseOrder_List" type="{urn:microsoft-dynamics-schemas/page/purchaseorder}PurchaseOrder_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "purchaseOrderList"
})
@XmlRootElement(name = "CreateMultiple_Result")
public class CreateMultipleResult {

    @XmlElement(name = "PurchaseOrder_List", required = true)
    protected PurchaseOrderList purchaseOrderList;

    /**
     * Gets the value of the purchaseOrderList property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderList }
     *     
     */
    public PurchaseOrderList getPurchaseOrderList() {
        return purchaseOrderList;
    }

    /**
     * Sets the value of the purchaseOrderList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderList }
     *     
     */
    public void setPurchaseOrderList(PurchaseOrderList value) {
        this.purchaseOrderList = value;
    }

}
