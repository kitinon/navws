package com.hiqfood.navws70;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthFilter implements Filter {

	private boolean logging;
	
	@Override
	public void doFilter(ServletRequest reqt, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) reqt;
		HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(true);

        Authenticator auth = new Authenticator(request, response); //use values from <env-entry> instead
        String authInfo = auth.getAuthInfo(true);
        if (logging) {
	        String logMsg = String.format("IP %s session %s ", request.getRemoteAddr(), session.getId());
	        if (authInfo != null) {
	        	System.out.println(logMsg + "is successfully authenticated.");
	        } else {
	        	System.err.println(logMsg + "could not be authenticated.");
	        }
        }
        if (authInfo != null) chain.doFilter(reqt, resp);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
        logging = config.getInitParameter("logging").equals("true");
	}

	@Override
	public void destroy() {
	}

}
