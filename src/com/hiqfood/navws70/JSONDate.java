package com.hiqfood.navws70;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSONDate {
	public static Date from(String dateString) {
		Pattern pattern = Pattern.compile("^/Date\\((.*)\\)/$");
	    Matcher matcher = pattern.matcher(dateString);
	    return (matcher.find() ?  new Date(Long.parseLong(matcher.group(1))) : null);
	}
}
