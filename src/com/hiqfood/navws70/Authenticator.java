package com.hiqfood.navws70;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Enumeration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Authenticator {
	private String authServer;
	private String authUrl;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;
	private String authInfo = null;
	private String responseBody = null;
	private Integer responseCode = null; 
	private boolean authenticated = false;
	private Socket authSocket = null;
	
	private void initialize() {
		this.session = request.getSession(true);
		String sessionAuthInfo = (String)session.getAttribute("authInfo");
		if (sessionAuthInfo != null) {
			authenticated = true;
			authInfo = sessionAuthInfo;
		}
	}
	
	public Authenticator(String server, String url, HttpServletRequest request, HttpServletResponse response) {
		this.authServer = server;
		this.authUrl = url;
		this.request = request;
		this.response = response;
		initialize();
	}

	public Authenticator(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		try {
			Context env = (Context)new InitialContext().lookup("java:comp/env");
			this.authServer = (String)env.lookup("AuthenticationServer");
			this.authUrl = (String)env.lookup("AuthenticationUrl");
			initialize();
		} catch (NamingException e) {
			System.err.println("Authenticator cannot read from <env-entry> in web.xml.");
			e.printStackTrace();
		}
	}
	
	private void CopyRequest() throws IOException {
		PrintWriter out = new PrintWriter(authSocket.getOutputStream());
		out.println("GET " + authUrl + " HTTP/1.1");
		out.println("Host: "+ authServer);
		out.println("Connection: keep-alive");
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) {
			String header = headers.nextElement();
			out.println("Authorization: " + header);
		}
		out.println();
		out.flush();
	}
	
	private void CopyResponse() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(authSocket.getInputStream()));
		String line;
		String firstLine = null;
		Integer contentLength = null; 
		while ((line = in.readLine()) != null) {
			if (firstLine == null) {
				firstLine = line;
				responseCode = Integer.parseInt(firstLine.split(" ")[1]);
			}
			if (line.equals("")) break;	// End of header section
			else  if (line.startsWith("WWW-Authenticate")) {
				String authString = line.split(":")[1].trim();
				response.addHeader("WWW-Authenticate", authString);
			} else if (line.startsWith("Content-Length")) {
				contentLength = Integer.parseInt(line.split(":")[1].trim());
			}
		}
		if (contentLength != null) {
			char[] buf = new char[contentLength];
			in.read(buf, 0, contentLength);
			responseBody = new String(buf);
			if (responseCode == 200) {
				authenticated = true;
				authInfo = responseBody;
			}
		}
	}

	public boolean authenticate() {
		if (authenticated) return true;
		authSocket = (Socket)session.getAttribute("AuthSocket");
		try {
			if (authSocket == null || authSocket.isClosed()) {
				authSocket = new Socket(authServer, 80);
				session.setAttribute("AuthSocket", authSocket);
			}
			CopyRequest();
			CopyResponse();
			if (authenticated) {
				session.setAttribute("authInfo", authInfo);
				authSocket.close();
				session.removeAttribute("AuthSocket");
				return true;
			}
			response.sendError(responseCode);
		} catch (IOException e) {e.printStackTrace();}
		return false;
	}
	
	public String getAuthInfo(boolean tryAuthenticate) {
		return authenticated || (tryAuthenticate && authenticate()) ? authInfo : null;
	}
	
	// same as getAuthInfo(false)
	public String getAuthInfo() {
		return authInfo;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public Integer getResponseCode() {
		return responseCode;
	}
}