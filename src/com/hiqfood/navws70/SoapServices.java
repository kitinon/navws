package com.hiqfood.navws70;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.ws.WebServiceException;

import schemas.dynamics.microsoft.page.customer.*;
import schemas.dynamics.microsoft.page.itemcard.*;
import schemas.dynamics.microsoft.page.plannedproductionorder.*;
import schemas.dynamics.microsoft.page.productionordercomponent.*;
import schemas.dynamics.microsoft.page.purchaseorder.*;
import schemas.dynamics.microsoft.page.purchasequote.*;
import schemas.dynamics.microsoft.page.reqworksheet.*;
import schemas.dynamics.microsoft.page.transferorder.*;
import schemas.dynamics.microsoft.page.vendor.*;

public class SoapServices {
    private final static String kuser = "hiqfood\\tomcat"; // your account name
    private final static String kpass = "Nurhaci1616"; // your password for the account
    private static class MyAuthenticator extends Authenticator {
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            // I haven't checked getRequestingScheme() here, since for NTLM
            // and Negotiate, the user name and password are all the same.
            // System.out.println("Feeding user name and password for " + getRequestingScheme());
            return (new PasswordAuthentication(kuser, kpass.toCharArray()));
        }
    }
    @SuppressWarnings("unused")
    private static CustomerService customerService = null;
    @SuppressWarnings("unused")
    private static ItemCardService itemCardService = null;
    @SuppressWarnings("unused")
    private static PlannedProductionOrderService plannedProductionOrderService = null;
    @SuppressWarnings("unused")
    private static ProductionOrderComponentService ProductionOrderComponentService = null;
    @SuppressWarnings("unused")
    private static PurchaseOrderService purchaseOrderService = null;
    private static PurchaseQuoteService purchaseQuoteService = null;
    private static ReqWorksheetService reqWorksheetService = null;
    @SuppressWarnings("unused")
    private static TransferOrderService transferOrderService = null;
    @SuppressWarnings("unused")
    private static VendorService vendorService = null;
    
	@SuppressWarnings("unused")
	private final static String CUSTOMER_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/Customer";
	@SuppressWarnings("unused")
	private final static String ITEM_CARD_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/ItemCard";
	@SuppressWarnings("unused")
	private final static String PLANNED_PRODUCTION_ORDER_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/PlannedProductionOrder";
	@SuppressWarnings("unused")
	private final static String PRODUCTION_ORDER_COMPONENT_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/ProductionOrderComponent";
	@SuppressWarnings("unused")
	private final static String PURCHASE_ORDER_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/PurchaseOrder";
	private final static String PURCHASE_QUOTE_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/PurchaseQuote";
	private final static String REQ_WORKSHEET_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/ReqWorksheet";
	@SuppressWarnings("unused")
	private final static String TRANSFER_ORDER_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/TransferOrder";
	@SuppressWarnings("unused")
	private final static String VENDOR_URL = "http://nav2013.hiqfood.com:7047/DynamicsNAV70/WS/HIQFOOD/Page/Vendor";
	
	static {
		//System.setProperty("file.encoding", "UTF-8"); // or MS874
    	Authenticator.setDefault(new MyAuthenticator());
    	// For use with Fiddler
		//System.setProperty("http.proxyHost", "127.0.0.1");
	    //System.setProperty("http.proxyPort", "8888");
    }
    	
    @SuppressWarnings("unchecked")
	static private <T> T getService(Class<T> serviceClass) {
        try {
    		if (serviceClass == ReqWorksheetService.class) {
    			URL url = new URL(REQ_WORKSHEET_URL);
    			if (reqWorksheetService == null) reqWorksheetService = new ReqWorksheetService(url);
    			return (T) reqWorksheetService;
    		} else if (serviceClass == PurchaseQuoteService.class) {
    			URL url = new URL(PURCHASE_QUOTE_URL);
    			if (purchaseQuoteService == null) purchaseQuoteService = new PurchaseQuoteService(url);
                return (T) purchaseQuoteService;
    		}
        } catch (MalformedURLException | WebServiceException e) {
			e.printStackTrace();
    	}
    	return null;
    }
    
	static public List<String> getPurchaseQuoteNos() {
		List<String> result = null;
		PurchaseQuotePort pqPort = getService(PurchaseQuoteService.class).getPurchaseQuotePort(); 
		PurchaseQuoteList pqList = pqPort.readMultiple(null, null, 0);
		for (PurchaseQuote pq : pqList.getPurchaseQuote()) {
			if (result == null) result = new ArrayList<String>();
			result.add(pq.getNo());
		}
		return result;
	}
	
	static public List<ReqWorksheet> getReqLines(Date dateFilter) {
		List<ReqWorksheetFilter> reqFilters = null;
		if (dateFilter != null) {
			reqFilters = new ArrayList<ReqWorksheetFilter>();
			ReqWorksheetFilter reqFilter = new ReqWorksheetFilter();
			reqFilter.setField(ReqWorksheetFields.DUE_DATE);
			//reqFilter.setCriteria("<"+(new SimpleDateFormat("MM-dd-yy")).format(dateFilter));
			reqFilter.setCriteria((new SimpleDateFormat("MM-dd-yy")).format(dateFilter));
			reqFilters.add(reqFilter);
		}
		
		ReqWorksheetPort reqPort = getService(ReqWorksheetService.class).getReqWorksheetPort();
		ReqWorksheetList reqList = reqPort.readMultiple("PR-HIQ", reqFilters, null, 0);
		return reqList.getReqWorksheet();
	}

	public static void main(String[] args) {
		for (String s : getPurchaseQuoteNos()) {
			System.out.println(s);
		}
	}
}