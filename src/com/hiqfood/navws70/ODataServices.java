package com.hiqfood.navws70;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

import org.apache.commons.io.IOUtils;

public class ODataServices {
	private final static String kuser = "hiqfood\\tomcat"; // your account name
    private final static String kpass = "Nurhaci1616"; // your password for the account
    private static class MyAuthenticator extends Authenticator {
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            // I haven't checked getRequestingScheme() here, since for NTLM
            // and Negotiate, the user name and password are all the same.
            // System.out.println("Feeding user name and password for " + getRequestingScheme());
            return (new PasswordAuthentication(kuser, kpass.toCharArray()));
        }
    }
    
    static {
		//System.setProperty("file.encoding", "UTF-8"); // or MS874
    	Authenticator.setDefault(new MyAuthenticator());
    	// For use with Fiddler
		//System.setProperty("http.proxyHost", "127.0.0.1");
	    //System.setProperty("http.proxyPort", "8888");
    }

    public static String getJSON(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/json");
		connection.connect();
		InputStream is = connection.getInputStream();
		StringWriter sw = new StringWriter();
		IOUtils.copy(is, sw);
		return sw.toString();
    }
    
	public static void main(String[] args) throws IOException {
		URL url = new URL("http://nav2013.hiqfood.com:7048/DynamicsNAV70/OData/Company('HIQFOOD')/PurchaseHistoryByReqLine");
		System.out.print(getJSON(url));
	}
}
