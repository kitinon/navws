package com.hiqfood.navws70;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

public class PurchaseUtil {
	static Gson gson = new Gson();

	static class RequisitionLine {
		public String text;
		public Boolean leaf = true;
		public String dueDate;
		public String worksheetTemplateName;
		public String journalBatchName; 
		public Long lineNo; 
		public String type; 
		public String no; 
		public String description;
		public Double quantity; 
		public String unitOfMeasureCode; 
		public Double directUnitCost;		

		public RequisitionLine(JSONObject jo) {
			try {
			    this.dueDate = jo.getString("Due_Date");
			    this.worksheetTemplateName = jo.getString("Worksheet_Template_Name");
			    this.journalBatchName = jo.getString("Journal_Batch_Name");
			    this.lineNo = jo.getLong("Line_No");
			    this.type = jo.getString("Type");
			    this.no = jo.getString("No");
			    this.description = jo.getString("Description");
			    this.quantity = jo.getDouble("Quantity");
			    this.unitOfMeasureCode = jo.getString("Unit_of_Measure_Code");
			    this.directUnitCost = jo.getDouble("Direct_Unit_Cost");
			    this.text = this.no + " " + quantity + " " + unitOfMeasureCode;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	static class RequisitionNode {
		public String text;
		public List<RequisitionLine> children;
		public RequisitionNode(String dueDate, List<RequisitionLine> children) {
			SimpleDateFormat ddMMMyyyy=new SimpleDateFormat("dd MMM yyyy");
			this.text = ddMMMyyyy.format(JSONDate.from(dueDate));
			this.children = children;
		}
	}
	
	private static String convert(JSONArray input) {
		try {
			Map<String, List<RequisitionLine>> map = new HashMap<>();
			List<RequisitionNode> nodes = new ArrayList<>();
			for (int i=0; i<input.length(); i++) {
				RequisitionLine r = new RequisitionLine(input.getJSONObject(i));
				List<RequisitionLine> list;
				if (map.containsKey(r.dueDate)) {
					list = map.get(r.dueDate);
				} else {
					list = new ArrayList<>();
					map.put(r.dueDate, list);
					nodes.add(new RequisitionNode(r.dueDate, list));
				}
				list.add(r);
			}
			JSONArray ja = new JSONArray(gson.toJson(nodes));
			JSONObject jo = new JSONObject();
			jo.put("sucess", true);
			jo.put("children", ja);
			return jo.toString();			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getRequisitionLines(String journalBatchName) {
		try {
			String urlText = "http://nav2013.hiqfood.com:7048/DynamicsNAV70/OData/Company('HIQFOOD')/RequisitionLine"; 
			if (journalBatchName != null) {
				urlText += ("?$filter=Journal_Batch_Name%20eq%20'" + journalBatchName + "'");
			}
			String jsonText;
			jsonText = ODataServices.getJSON(new URL(urlText));
			JSONObject jo = new JSONObject(jsonText);
			JSONObject d = jo.getJSONObject("d");
			JSONArray results = d.getJSONArray("results");
			String output = convert(results);
			return output;
		} catch (IOException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(getRequisitionLines("PR-HIQ"));
	}
}